<html lang="en">
	<head>
		<title>mathbox.js</title>
		<meta charset="utf-8">
		<!--script src="static/js/jquery-3.4.1.min.js"></script-->
    <script src="static/js/jquery-3.4.1.min.js"></script>
    <script src="static/js/plotly.min.js"></script>
		
	</head>
	<body>
<!-- Create a div where the graph will take place -->
<div id="myDiv"></div>

<script>

var n = Date.now();

var layout = {
  xaxis: {
    type: 'date',
    title: 'Time in Milliseconds',
    tick0: String(n),
    dtick: 1000
  },
  yaxis: {
    title: 'Random Value 0-1'
  },
  title:'Random Values over Time(ms)'
};

var trace1 = {
  type: 'scatter',
  x: [n],
  y: [Math.random()],
  mode: 'lines'
};

var trace2 = {
  type: 'scatter',
  x: [n],
  y: [Math.random()],
  mode: 'lines'
};

var data = [trace1, trace2];

Plotly.newPlot('myDiv', data, layout);



function updateFunc(d) {
  var myDiv = document.getElementById('myDiv');
  var n = Date.now();
  Plotly.extendTraces('myDiv', {
    x: [[n],[n] ],
    y: [[Math.random(20)], [Math.random(20)]]
  }, [0, 1])

}
var myVar = setInterval(function() { updateFunc(data); }, 1000);
   
    </script>
    </body>
    </html>