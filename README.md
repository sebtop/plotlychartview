# PlotlyChartView

Datenübertragung mit websocket von Bridge zu Frontend (webapp)

Donnerstag, 23. Januar 2020
15:18

- Google protobuf 3 als binär verschlüsseltes effizientes Übertragungsprotokoll / Alternative Json textbasiertes Datenformat
- Prototyp (.proto Datei als Definition) gilt für einen einzelnen Parameter. 
- Parameter Datensatz kann auch trace genannt werden.
- Im Graph-Plot können viele Datensätze (Traces) dargestellt werden.
- Traces werden in getrennten Instanzen übertragen.
- Trace-Datenpaket enthält Information über Trace-Identität
- Datensatz bestehend aus Liste mit Zeitstempeln und Liste mit Datenpunkten
