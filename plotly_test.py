# -*- coding: utf-8 -*-
import sys, os, time, json, sqlite3, threading, re, random
from bottle import Bottle, get, post, static_file, run, template, request, response, route, TEMPLATE_PATH
from bottle.ext.websocket import GeventWebSocketServer
from geventwebsocket.exceptions import WebSocketError
from bottle.ext.websocket import websocket
from dataRunnerThread import dataRunnerThread
from gevent import socket
import queue as queue

users = set()
path = os.path.abspath(__file__)
print(path)
dir_path = os.path.dirname(path)
global TEMPLATE_PATH
TEMPLATE_PATH.insert(0, dir_path+'/views')

queueLock = threading.Lock()	    #queueLock to prevent blocking if both reader and writer attempt to access the queue
MlcReceiveQueue = queue.Queue(1)    #queue for the received parameter, passing parameter between threads


try:
    DataRunnerThread = opcuaReceiveThread(MlcReceiveQueue, dictConfig['url'], dictOfPages) #"opc.tcp://10.104.53.40:4840"
except NameError:                                                                         #"opc.tcp://192.168.1.10:4840"
    print('DataRunner did not start') #Error Message for console

try:
    DataRunnerThread.daemon = True #mark thread as backround deamon to run independend
    DataRunnerThread.start()    #start deamon
except:
    print('DataRunner crashed')
    pass


'''
def initDataBase(dbPath):
    # Temperatur-Werte für erste Inbetriebnahme    
    time = time.time() # Messbereich Sensor Untergerenze
    posZyl1 = 0.0 # Messbereich Sensor Obergrenze
    posZyl2 = 0.0 # Benutzerdefinierte Untergrenze der Temperatur
    
    connection = sqlite3.connect(dbPath)
    cursor = connection.cursor()
    # Tabelle erzeugen
    sql = "CREATE TABLE trendingTable("\
        "time FLOAT, posZyl1 FLOAT, posZyl2 FLOAT)" 
    cursor.execute(sql)
    # Werte für erste Inbetriebnahme
    sql = "INSERT INTO trendingTable VALUES(" + str(time) + ", " \
           + str(posZyl1) + ", " \
           + str(posZyl2) + ")"
    cursor.execute(sql)
    connection.commit()
    connection.close()
    print('Datenbank trendingTable.db mit ', sql , ' Inhalt angelegt')

def feedRandomData(dbPath):

    connection = sqlite3.connect(dbPath)
    cursor = connection.cursor()
    sql = "INSERT INTO trendingTable VALUES(" + str(time.time()) + ", " \
           + str(random.random()) + ", " \
           + str(random.random()) + ")"
    cursor.execute(sql)
    connection.commit()
    connection.close()


def readAllData(dbPath):
    connection = sqlite3.connect(dbPath)
    cursor = connection.cursor()
    sql = "SELECT * FROM (\
    SELECT * FROM trendingTable LIMIT 10)"
    cursor.execute(sql)
    rows = cursor.fetchall()
    for row in rows:
        print(row)
'''
users = set()
path = os.path.abspath(__file__)
print(path)
dir_path = os.path.dirname(path)
print(dir_path)

@route('/static/:path#.+#')
def static_files(path):
    print(dir_path+'/'+'static/')
    return static_file(path, root=dir_path+'/'+'static/') 

@route('/')
def graph():
    return template('graph')

#run(host='127.0.0.1', port=8080, server=GeventWebSocketServer)
run(host='127.0.0.1', port=8080, reloader=True, debug=True)