# coding: utf-8
import queue as queue
import threading
import time
import json

class dataRunnerThread(threading.Thread): #"opc.tcp://10.104.53.40:4840"
    def __init__(self, receiveQueue, url, varDict):
        threading.Thread.__init__(self)
        self._queueLock = threading.Lock()
        self._rqueue = receiveQueue
        self.plotData = 0
        
    def run(self):
        
        while(not self.stop_event):
            #Daten berechnen/erzeugen/holen

            #Alle Daten in Queue schieben und in Main-Thread schicken
            self._queueLock.acquire()
            if not self._rqueue.empty():
                dummy = self._rqueue.get(True, 0.05) #Empty the queue and dismiss whatever is in it
                self._rqueue.put(self.plotData)
            else:
                self._rqueue.put(self.plotData)
            self._queueLock.release()
    
    def stop(self):
        self.stop_event = True
        print('thread gets closed')    